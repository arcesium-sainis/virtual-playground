const express = require("express"),
      app =express(),
      bodyParser = require("body-parser");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.set("view engine","ejs")
app.use(express.static(__dirname + "/public"));


app.get('/', function (req, res) {
  res.render('index.ejs')
})

app.get('/snake', function (req, res) {
    res.render('snake.ejs')
  })

app.get('/flappy', function (req, res) {
    res.render('flappy.ejs')
});
  

app.listen("8080",function(){
    console.log("Server started At port 8080");
});